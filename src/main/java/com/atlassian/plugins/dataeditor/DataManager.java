package com.atlassian.plugins.dataeditor;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class DataManager
{
    private final PluginSettingsFactory pluginSettingsFactory;

    public DataManager(PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    public Object getGlobalPluginSetting(String dataKey)
    {
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        return pluginSettings.get(dataKey);
    }

    public Object getKeyedPluginSetting(String settingsKey, String dataKey)
    {
        PluginSettings pluginSettings = pluginSettingsFactory.createSettingsForKey(settingsKey);
        return pluginSettings.get(dataKey);
    }
}

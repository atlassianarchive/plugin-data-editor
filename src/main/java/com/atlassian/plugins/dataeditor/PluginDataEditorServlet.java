package com.atlassian.plugins.dataeditor;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.templaterenderer.TemplateRenderer;

public class PluginDataEditorServlet extends HttpServlet
{
    private final TemplateRenderer renderer;
    private final ApplicationProperties applicationProperties;
    private final LoginUriProvider loginUriProvider;
    private final UserManager userManager;
    private final WebSudoManager webSudoManager;

    public PluginDataEditorServlet(TemplateRenderer renderer, ApplicationProperties applicationProperties, LoginUriProvider loginUriProvider, UserManager userManager, WebSudoManager webSudoManager)
    {
        this.renderer = renderer;
        this.applicationProperties = applicationProperties;
        this.loginUriProvider = loginUriProvider;
        this.userManager = userManager;
        this.webSudoManager = webSudoManager;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        try
        {
            // Enable web sudo protection if needed and if the app we are running in supports it
            webSudoManager.willExecuteWebSudoRequest(request);

            if (!userManager.isSystemAdmin(userManager.getRemoteUsername(request)))
            {
                redirectToLogin(request, response);
                return;
            }

            response.setContentType(MediaType.TEXT_HTML);

            Map<String, Object> context = new HashMap<String, Object>();
            context.put("baseUrl", applicationProperties.getBaseUrl());

            renderer.render("plugin-data-editor.vm", context, response.getWriter());
        }
        catch (WebSudoSessionException wse)
        {
            webSudoManager.enforceWebSudoProtection(request, response);
        }
    }

    // ************************************************************************************************************************************
    // COPIED FROM UPM
    // ************************************************************************************************************************************

    static final String JIRA_SERAPH_SECURITY_ORIGINAL_URL = "os_security_originalurl";
    static final String CONF_SERAPH_SECURITY_ORIGINAL_URL = "seraph_originalurl";

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        final URI uri = getUri(request);
        addSessionAttributes(request, uri.toASCIIString());
        response.sendRedirect(loginUriProvider.getLoginUri(uri).toASCIIString());
    }

    private URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private void addSessionAttributes(final HttpServletRequest request, final String uriString)
    {
        // UPM-637 - Seraph tries to be clever and if your currently logged in user is trying to access a
        // URL that does not have a Seraph role restriction then it will redirect to os_destination. In the case
        // of the UPM we do not want this behavior since we do have an elevated Seraph role but have not way to
        // programatically tell Seraph about it.
        // UPM-637 - this is the JIRA specific string to let Seraph know that it should re-show the login page
        request.getSession().setAttribute(JIRA_SERAPH_SECURITY_ORIGINAL_URL, uriString);
        // UPM-637 - this is the Confluence specific string to let Seraph know that it should re-show the login page
        request.getSession().setAttribute(CONF_SERAPH_SECURITY_ORIGINAL_URL, uriString);
    }

    // ************************************************************************************************************************************
}

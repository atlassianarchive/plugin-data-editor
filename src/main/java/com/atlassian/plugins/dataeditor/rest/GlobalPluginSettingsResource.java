package com.atlassian.plugins.dataeditor.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.dataeditor.DataManager;

@Path("/{dataKey}")
public class GlobalPluginSettingsResource
{
    private final DataManager dataManager;

    public GlobalPluginSettingsResource(DataManager dataManager)
    {
        this.dataManager = dataManager;
    }

    @GET
    @Produces("application/json")
    public Response get(@PathParam("dataKey") String dataKey)
    {
        Object setting = dataManager.getGlobalPluginSetting(dataKey);
        if (setting == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(setting).build();
    }
}

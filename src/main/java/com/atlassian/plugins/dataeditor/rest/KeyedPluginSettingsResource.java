package com.atlassian.plugins.dataeditor.rest;

import com.atlassian.plugins.dataeditor.DataManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/{settingsKey}/{dataKey}")
public class KeyedPluginSettingsResource
{
    private final DataManager dataManager;

    public KeyedPluginSettingsResource(DataManager dataManager)
    {
        this.dataManager = dataManager;
    }

    @GET
    @Produces("application/json")
    public Response get(@PathParam("settingsKey") String settingsKey, @PathParam("dataKey") String dataKey)
    {
        Object setting = dataManager.getKeyedPluginSetting(settingsKey, dataKey);
        if (setting == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(setting).build();
    }
}
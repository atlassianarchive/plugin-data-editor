function LookupResult(dataKey, value, settingsKey) {
    this.dataKey = dataKey;
    this.value = value;
    this.settingsKey = settingsKey || '';

    this.displayKeysHtml = '<span id="data-key-result" class="pluginsettings-key">' + dataKey + '</span>' +
                           ((settingsKey && settingsKey != '') ? ' (in <span id="settings-key-result">' + settingsKey + '</span>)' : '');
    this.displayKeysNonHtml = dataKey + ((settingsKey && settingsKey != '') ? ' (in ' + settingsKey + ')' : '');
}

function redoLookup(dataKey, settingsKey) {
    // toggle settings type dropdown & settings key field
    var settingsKeySelected = (AJS.$('#data-type option:selected').val() == "keyed-ps");
    if (settingsKey && settingsKey != '') {
        if(!settingsKeySelected) {
            AJS.$('#data-type').val("keyed-ps");
        }
    }
    else {
        if(settingsKeySelected) {
            AJS.$('#data-type').val("global-ps");
        }
    }
    AJS.$('#settings-input').toggle(AJS.$('#data-type option:selected').val() == "keyed-ps");

    AJS.$('#settings-key-text').val(settingsKey);
    AJS.$('#data-key-text').val(dataKey);

    AJS.$('#data-key-submit').click();
}

function addToRecentLookupsList(lookupResult)
{
    if ((jQuery.trim(lookupResult.value)) == '' || '<i>No lookups yet.</i>' === jQuery.trim(lookupResult.dataKey) ) { // TODO why can't i use AJS.$.trim?
        return;
    }

    var lookupList = AJS.$('#recent-lookups');
    // if size is at least five, then remove oldest stored value
    var historySize = AJS.$('li', lookupList).length;
    if (historySize >= 5) {
        AJS.$('li:last', lookupList).remove();
    }
    lookupList.prepend('<li><span class="pluginsettings-key">' + lookupResult.displayKeysNonHtml + '</span><br><span class="pluginsettings-value">' + lookupResult.value
            + '</span> (<a href="#" onClick="redoLookup(\''+ lookupResult.dataKey + '\', \'' + lookupResult.settingsKey + '\');return true">redo</a>)</li>');
}

function showResult(lookupResult)
{
    var dataKeysElement = AJS.$('#data-keys');
    var dataKeyElement = AJS.$('#data-key-result');
    var settingsKeyElement = AJS.$('#settings-key-result');
    var redoButton = AJS.$('#data-redo-last');
    var container = AJS.$("#lookup-result-container");
    
    var prevLookupResult = new LookupResult(dataKeyElement.html(), container.html(), settingsKeyElement ? settingsKeyElement.html() : '');

    dataKeysElement.hide().html(lookupResult.displayKeysHtml);
    container.html(lookupResult.value);
    redoButton.attr("onClick","redoLookup('" + lookupResult.dataKey + "','" + lookupResult.settingsKey + "');return true");

    // animate back with the new results
    dataKeysElement.fadeIn();
    container.fadeIn();
    redoButton.fadeIn();

    addToRecentLookupsList(prevLookupResult);
}

function clearData() {
    AJS.$('#data-key-text').val("");
    AJS.$('#settings-key-text').val("");
}

function getResult(data) {
    if (data == null || data == "")
    {
        result = '<i>Value for key not found</i>';
    }
    else
    {
        result = data.toString();
    }
    return result;
}

AJS.$(document).ready(function() {

    AJS.$("#keyed-ps").change(function() {
       AJS.$("#settings-key-group").toggle(AJS.$(this).attr("checked"));
    });

    AJS.$("#global-ps").change(function() {
        AJS.$("#settings-key-group").toggle(!AJS.$(this).attr("checked"));
    });

    // TODO set selection based on what is currently selected (global isn't default if changed before and page refreshed)

    // make pushing enter in the key input field submit the form
    AJS.$('#data-key-text').bind('keyup', function(e) {
        if(e.keyCode==13){
            AJS.$('#data-key-submit').click();
        }
    });

    // setting up clicking the lookup button to call to our rest resource
    AJS.$('#data-key-submit').click(function(){
        var dataKey = AJS.$('#data-key-text').val();
        var settingsKey = AJS.$('#settings-key-text').val();
        var url = AJS.$('#baseUrl').val() + '/rest/pde/1.0/' + (settingsKey != '' ? settingsKey + '/' : '') + dataKey;
        jQuery.ajax({  // TODO why can't i use AJS.$.ajax?
            url: url,
            // using complete instead of success and error, because i was getting a parsererror sometimes even though the status was OK
            complete: function(xhr, status) {
                var resultText;
                if (status === 'error' || !xhr.responseText) {
                    if (xhr.status === 404) {
                        resultText = '<i>No value found.</i>';
                    } else {
                        resultText = '<i>Uh oh, something went wrong</i>';
                    }
                }
                else {
                    resultText = getResult(xhr.responseText);
                }
                showResult(new LookupResult(dataKey, resultText, settingsKey));
                clearData();
            }
        });
    });
});
